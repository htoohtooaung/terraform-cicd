resource "aws_lambda_permission" "this" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.this.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_api_gateway_rest_api.this.execution_arn}/*/*"
}

# TO FILL IN resources, including but not limited to
# - aws_api_gateway_rest_api
# - aws_api_gateway_method
# - aws_api_gateway_deployment

resource "aws_api_gateway_rest_api" "this" {
  name        = "restapi"
  description = "HelloWorld Lambda"
}


resource "aws_api_gateway_resource" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "this" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_rest_api.this.root_resource_id
  http_method   = "ANY"
  authorization = "NONE"
  # api_key_required = true
}

resource "aws_api_gateway_integration" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_method.this.resource_id
  http_method = aws_api_gateway_method.this.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.this.invoke_arn
}

resource "aws_api_gateway_deployment" "this" {
  depends_on = [aws_api_gateway_integration.this]


  rest_api_id = aws_api_gateway_rest_api.this.id
  stage_name  = "test"
}

# #API Key
resource "aws_api_gateway_api_key" "this" {
  name        = "Hlloworld-localstack-test"
  description = "API key for API Gateway"
  enabled     = true

}
#custom domain

resource "aws_api_gateway_domain_name" "this" {
  # domain_name              = aws_acm_certificate.this.domain_name
  domain_name = "helloworld.myapp.earth" # initially htoohtoo.cloud
  # regional_certificate_arn = aws_acm_certificate.this.arn


  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "this" {
  api_id      = aws_api_gateway_rest_api.this.id
  domain_name = aws_api_gateway_domain_name.this.domain_name
  stage_name  = aws_api_gateway_deployment.this.stage_name
}

resource "aws_api_gateway_api_key" "MyDemoApiKey" {
  name = "helloworld-localstack-test"
}